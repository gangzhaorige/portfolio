/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */
const functions = require('firebase-functions')
let config = require('./env.json')
functions.config()
module.exports = {
	/* Your site config here */

	siteMetadata: {
		author: 'Gangzhaorige Li',
		mongolianName: 'Ganzorig',
		email: 'gangzhaorige.li@gmail.com',
		facebook: 'https://www.facebook.com/ganzorig.li',
		linkedin: 'https://www.linkedin.com/in/gangzhaorige-li/',
		github: 'https://github.com/settings/profile',
		site: 'gl',
		phone: '650-436-2846',
		address: '3655 Colegrove Street, Apt 28, San Mateo, CA 94403'
	},
	plugins: [
		{
			resolve: 'gatsby-source-contentful',
			options: {
				accessToken: config.service.access_token,
				spaceId: config.service.spaceid,
			}
		},
		'gatsby-plugin-sass',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'src',
				path: `${__dirname}/src/`
			}
		},
		'gatsby-plugin-sharp',
		{
			resolve: 'gatsby-transformer-remark',
			options: {
				plugins: [
					'gatsby-remark-relative-images',
					{
						resolve: 'gatsby-remark-images',
						options: {
							maxWidth: 750,
							linkImagesToOriginal: false
						}
					}
				]
			}
		},
		'gatsby-plugin-react-helmet',
		'gatsby-plugin-fontawesome-css',
		{
			resolve: `gatsby-plugin-styled-components`,
		},
	]
}
