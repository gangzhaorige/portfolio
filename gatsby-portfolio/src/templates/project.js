import React from 'react'
import Layout from '../components/layout'
import {graphql} from 'gatsby'
import Head from '../components/head'
import styled from 'styled-components'
import { Card } from 'antd'
export const query = graphql`
    query($slug: String!){
        markdownRemark(fields: { slug: {eq: $slug}}){
            frontmatter{
                projectName
                year
            }
            html
        }
    }
`

const Project = (props) => {
    
    return (
        <Layout>
            <Head title = {props.data.markdownRemark.frontmatter.projectName}/>
            <div className = {props.className}>
                <Card title={props.data.markdownRemark.frontmatter.projectName} bordered={false}  >
                    <div className = "inner">
                        <div className = "inside" dangerouslySetInnerHTML={{ __html: props.data.markdownRemark.html}}></div>
                    </div>
                </Card>
            </div>
        </Layout>
    )
}

export default styled(Project)`
    display: flex;
    width: 100%;
    flex-direction: column;
    margin-top: 15%;
    justify-content: center;
    align-items: center;
    h1, p{
        text-align: center;
    }
    .ant-card-body{
        display: flex;
        flex-wrap: wrap;
    }
    .ant-card{
        width: 500;
        texta-align: center;
    }
    .inner{
        display:flex;
        .inside{
            ol{
                display: flex;
                flex-direction: column;
                list-style: none;
                text-align: left;
                margin: 0;
                padding: 0;
            }
        }
    }
    @media only screen and (max-width: 500px) {
        .ant-card{
            width: 300px;
        }
    }
    .ant-card-head-title{
        text-align: center;
    }
`