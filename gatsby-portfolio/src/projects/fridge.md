---
projectName: "FridgeApp9000"
year: "2019"
path: "./fridge.png"
---
A fridge App that manages content.

1. A mobile web application that manages fridge and its contents.
2. Implemented receipt capture that allows users to add the scanned items into their fridges using Google Cloud Vision API.
3. Implemented functional requirements, such as adding and removing items from the fridge and adding friends to thefridges via email.
4. Technologies used: AWS ec2 and RDS, Apache, Python, Django, HTML, CSS, and Bootstrap.