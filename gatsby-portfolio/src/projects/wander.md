---
projectName: "Wander"
year: "2019"
path: "./wander.png"
---

Online Multiplayer Game

1. WANDER is a cooperative side-scrolling action RPG.
2. Designed and Implemented Login and Registration UI and its functionalities.
3. Developed multiplayer functionality using Mirror Networking.
4. Set up the Dedicated Server and MySQL database.
5. [Link](https://drive.google.com/file/d/10vrveXTTlkdmst_ELEhfIVFSnySIGSZu/view?usp=sharing)