---
projectName: "Uno Web Game"
year: "2019"
path: "./uno.png"
---
Online Multiplayer Uno


1. A simple Uno game that supports any number of players.
2. Built lobby system that allows players to join and start the game.
3. Implemented global chat and lobby/in-game chat using socket.io.
4. Technologies used: Node Js, Express, npm, Pug, Css, and Postgres.
5. [Link](https://uno-667-02.herokuapp.com/index)