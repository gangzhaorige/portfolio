import React from 'react'
import Layout from '../components/layout'
import { Link, graphql, useStaticQuery } from 'gatsby'
import Head from '../components/head'
import { Card } from 'antd';
import 'antd/dist/antd.css';
import styled from 'styled-components';
const { Meta } = Card;
const ProjectPage = (props) => {
	const data = useStaticQuery(graphql`
		query{
			allMarkdownRemark{
				edges {
					node{
						frontmatter{
							projectName
							year
							path{
								name
							}
						}
						fields{
							slug
						}
					}
				}
			}
		}
	`)
	return (
		<Layout>
			<Head title="Projects" />
			<div className={props.className}>
				<h1>Projects</h1>
				<div className="project-container">
					{data.allMarkdownRemark.edges.map((edge, index) => {
						return (
							<div key={index}>
								<Link to={`/project/${edge.node.fields.slug}`}>
									<Card
										hoverable
										style={{width: 240, marginBottom: 16, marginLeft: 16, marginRight: 16}}
										cover={<img alt="example" height = "300px" src={require(`../projects/${edge.node.frontmatter.path.name}.png`)} />}>
											
										<Meta title={edge.node.frontmatter.projectName} description={edge.node.frontmatter.year} />
									</Card>
								</Link>
							</div>
						)
					})}
				</div>
			</div>

		</Layout>
	)
}
export default styled(ProjectPage)`
	padding-top: 32px;
	text-align: center;
    h1{
        color:white;
    }
    li{
		text-decoration:none;
	}
	.project-container{
		flex-wrap: wrap;
		display: flex;
		width: 100%;
		padding: 16px;
		justify-content: center;
	}
`