import React from 'react'
import Layout from '../components/layout'
import { graphql, useStaticQuery } from 'gatsby'
import Head from '../components/head'
import styled from 'styled-components';
import { Progress } from 'antd';
const SkillPage = (props) =>  {
    const data = useStaticQuery(graphql`
        query{
            allContentfulSkills(sort: {fields: level, order: DESC}){
                totalCount
                edges{
                    node{
                        type
                        level
                    }
                }
            }
            allContentfulLanguage(sort:{fields:level, order: DESC}){
                edges{
                    node{
                        type
                        level
                    }
                }
            }
        }
    `)
  
    return (
        <Layout>
            <Head title="Skills" />
            <div className={props.className}>
                <div className="container">
                    <h1>Technical Level</h1>
                    {data.allContentfulSkills.edges.map((edge,index) => {
                        return(
                            <div className="bar" key = {index}>
                                <p>{edge.node.type} </p> 
                                <Progress percent={edge.node.level} status="active" strokeColor={{from: '#593780' ,to: '#1f006e'}} />
                            </div>
                        )
                    })}
                    <h1>Language Level</h1>
                    {data.allContentfulLanguage.edges.map((edge,index) => {
                        return(
                            <div className="bar" key = {index}>
                                <p>{edge.node.type} </p> 
                                <Progress percent={edge.node.level} status="active" strokeColor={{from: '#593780' ,to: '#1f006e'}} />
                            </div>
                        )
                    })}
                </div>
            </div>
        </Layout>
    )
}

export default styled(SkillPage)`
    flex-direction: column;
    margin-top: 5%;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    .container{
        width: 700px;
        min-width:350px;
        .bar{
            display: flex;
            p{
                width: 150px;
                color: white;
            }
           
        }
    }
    h1 {
        color: white;
        font-size: 20px;
    }
    .ant-progress-text{
        color: white;
    }
    @media only screen and (max-width: 700px) {
        .container {
            max-width: 350px;
            min-width: 200px;
            width: 300px;
        }
    }
`