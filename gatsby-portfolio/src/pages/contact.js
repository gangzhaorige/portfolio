import React from 'react'
import Layout from '../components/layout'
import Head from '../components/head'
import styled from 'styled-components';
import ContactCard from '../components/contactCard'
const ContactPage = (props) => {
	
	return (
		<Layout>
			<Head title="Contact" />
			<div className={props.className}>
				<ContactCard/>
			</div>
		</Layout>
	)
}

export default styled(ContactPage)`
	display: flex;
	width: 100%;
	justify-content: center;
	align-items: center;
	height: 80vh;
	margin:0;
	
	.contact{
		display: flex;
		align-items: center;
		margin-bottom: 10px;
	}
	.logo{
		margin-right: 7px;
	}
	.paragraph{
		text-align: left;
	}
	.card-object{
		margin: 1px;
	}
`