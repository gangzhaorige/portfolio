import React from 'react'
import Layout from '../components/layout'
import Head from '../components/head'
import styled from 'styled-components';
import 'font-awesome/css/font-awesome.min.css';
import Ganzorig from '../../static/ganzorig.png'
import {graphql, useStaticQuery } from 'gatsby'

const Index = (props) => {
	const data = useStaticQuery(graphql`
        query{
            site{
                siteMetadata{
                    author
                }
            }
        }
    `)
	return (
		<Layout>
			<Head title="Home" />
			<div className={props.className}>
				<div className="container">
					<div className="circle"></div>
					<h1>{data.site.siteMetadata.author} | Software Engineer</h1>
					<p>
						Motivated software engineering graduate from San Francisco State University recently.
						Currently looking for Software Engineer position. I have experience with Java, C++, Python,
						JavaScript, MySql, Postgres, Django Framework, Docker, AWS ec2 and Rds, Html, and CSS.
						I love to create and manage computer programs, work with the clients plan and, design the software for the target audience.
						I enjoy teamwork. I love solving interesting problems. I know multiple languages: English, Mandarin, Mongolian, Russian
					</p>
				</div>
			</div>
		</Layout>
	)
}
export default styled(Index)`
	margin-top: 10%;
	display: flex;
	justify-content: center;
	align-items: center;
	.container{
		max-width:700px;
		min-width:300px;
		padding: 8px;
	}

	.circle{
		width: 175px;
		height: 175px;
		background-image: url('${Ganzorig}');
		border-radius: 50%;
		float: left;
		shape-outside: circle();
		background-position: center;
		margin: 35px 20px 20px 0;
		background-size: cover;
		background-color: white;
	}
	p{
		color: rgba(255,255,255,0.8);
		margin: 0;
		padding: 0;
		text-align: justify;
		line-height: 22px;
		font-size: 15px;
	}
	h1{
		color: rgba(255,255,255,0.8);
		margin: 0 0 0;
		padding: 0;
		font-size: 25px;
	}
`

