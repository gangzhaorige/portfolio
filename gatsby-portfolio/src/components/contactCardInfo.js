import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
const ContactCardInfo = ({className, logo, information}) => {
    
    return (
        <div className={className}>
            <div className="contact">
                <div className="logo">
                    <FontAwesomeIcon icon={logo} size="2x" />
                </div>
                <div className="paragraph">
                    {information}
			    </div>
            </div>
        </div>
    )
}

export default styled(ContactCardInfo)`
    .contact{
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }
    .logo{
        margin-right: 7px;
    }
    .paragraph{
        text-align: left;
    }
    .card-object{
        margin: 1px;
    }
`
