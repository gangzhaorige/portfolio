import { Link, graphql, useStaticQuery } from 'gatsby'
import styled from 'styled-components';
import React from 'react'


const Header = (props) => {

    function change() {  
        let mainNav = document.getElementById('js-menu');
        mainNav.classList.toggle('active');
    }
    const data = useStaticQuery(graphql`
    query{
        site{
            siteMetadata{
                author
            }
        }
    }
    `)
    return (
        <div className={props.className}>
            <nav className="navbar">
                <span className="navbar-toggle">
                    <i className="fas fa-bars" onClick={change} ></i>
                </span>
                <Link to="/" className="logo">{data.site.siteMetadata.author}</Link>
                <ul className="main-nav" id="js-menu">
                    <li>
                        <Link to="/" className="nav-links ">Home</Link>
                    </li>
                    <li>
                        <Link to="/skill" className="nav-links ">Skills</Link>
                    </li>
                    <li>
                        <Link to="/project" className="nav-links ">Projects</Link>
                    </li>
                    <li>
                        <Link to="/contact/" className="nav-links">Contact</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );

}

export default styled(Header)`
* {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
}
.navbar {
    font-size: 18px;
    padding-bottom: 10px;
}
.main-nav {
    list-style-type: none;
    display: none;
}
.nav-links,
.logo {
    text-decoration: none;
    color: rgba(255, 255, 255, 0.7);
}
.main-nav li {
    text-align: center;
    margin: 15px auto;
}
.logo {
    display: inline-block;
    font-size: 22px;
    margin-top: 10px;
    margin-left: 20px;
}
.navbar-toggle {
    position: absolute;
    top: 10px;
    right: 20px;
    cursor: pointer; 
    color: rgba(255,255,255,0.8);
    font-size: 24px;
}
.main-nav {
    list-style-type: none;
    display: none;
}
.active {
    display: block;
}
@media screen and (min-width: 768px) {
    .navbar {
        display: flex;
        justify-content: space-between;
        padding-bottom: 0;
        height: 60px;
        align-items: center;
    }
    .main-nav {
        display: flex;
        margin-right: 30px;
        flex-direction: row;
        justify-content: flex-end;
    }
    .main-nav li {
        margin: 0;
    }
    .nav-links {
        margin-left: 40px;
    }
    .logo {
        margin-top: 0;
    }
    .navbar-toggle {
       display: none;
    }
    .logo:hover,
    .nav-links:hover {
        color: rgba(255, 255, 255, 1);
    }
}
.line1, .line2, .line3 {
    margin-top:5px;
    background-color:#fff;
    width:25px;
    height:3px;
    display:block;
    position:relative;
    opacity:1.0;
    border-radius:15%;
    transition: all .3s;
  }
`