import React from 'react'
import Footer from './footer'
import Header from './header'
import styled from 'styled-components'
import '../styles/global.css'

const Layout = (props) => {
    return (
        <div className={props.className}>
            <Header />
            {props.children}
            <div className="space">
            </div>
            <Footer />
        </div>
    )
}

export default styled(Layout)`
    margin: 0;
    .space{
        height: 100px;
    }
`