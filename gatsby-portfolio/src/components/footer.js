import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faGithub, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
const Footer = (props) => {
    const data = useStaticQuery(graphql`
        query{
            site{
                siteMetadata{
                    author
                }
            }
        }
    `)
    return (
        <footer className={props.className}>
            <div className="logos">
                <a href="https://www.facebook.com/ganzorig.li" target="_blank" rel="noreferrer" aria-label="Save"><FontAwesomeIcon icon={faFacebookF} size="3x" inverse /></a>
                <a href="https://www.linkedin.com/in/gangzhaorige-li/" target="_blank" rel="noreferrer" aria-label="Save"><FontAwesomeIcon icon={faLinkedinIn} size="3x" inverse /></a>
                <a href="https://github.com/gangzhaorige" target="_blank" rel="noreferrer" aria-label="Save"><FontAwesomeIcon icon={faGithub} size="3x" inverse /></a>
                <p>Created by {data.site.siteMetadata.author}</p>
            </div>
        </footer>
    )
}

export default styled(Footer)`
    bottom: 0;
    left: 0;
    width: 100%;
    color: white;
    position: fixed;
    text-align: center;
    a{
        padding: 25px;
    }
    background-image: linear-gradient(90deg,#527cd0 0%, #3fd9d7 100%);
`
