import React from 'react'
import styled from 'styled-components'
import { Card } from 'antd'
import { faAddressCard, faEnvelope, faUserCircle } from '@fortawesome/free-regular-svg-icons';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import {graphql, useStaticQuery } from 'gatsby'
import ContactCardInfo from './contactCardInfo'
const ContactCard = (props) => {
    const data = useStaticQuery(graphql`
        query{
            site{
                siteMetadata{
                    author
					address
					email
					phone
                    mongolianName
                }
            }
        }
    `)
    return (
        <div className={props.className}>
            <div className="card-wrapper">
                <div className="card">
                    <Card className="card-object" title="Cantact Info" bordered={false} style={{ width: 300, textAlign: "center" }}>
                        <ContactCardInfo information = {data.site.siteMetadata.author + ' (' + data.site.siteMetadata.mongolianName + ')'} logo = {faUserCircle}/>
                        <ContactCardInfo information = {data.site.siteMetadata.address} logo = {faAddressCard}/>
                        <ContactCardInfo information = {data.site.siteMetadata.email} logo = {faEnvelope}/>
                        <ContactCardInfo information = {data.site.siteMetadata.phone} logo = {faPhone}/>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default styled(ContactCard)`
    .card-wrapper{
        padding: 1rem;
        position: relative;
        background: linear-gradient(to right, #e6b7c1, #1f006e);
        padding: 3px;
    min-width: 300px;
    }
    .card{
        min-width: 40%;
    }
    .card-object{
		margin: 1px;
	}
`
